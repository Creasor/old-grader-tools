import java.awt.event.*
import java.nio.charset.Charset
import java.util.*
import javax.swing.*
import javax.swing.border.EmptyBorder
import kotlin.math.max

group = "edu.vanderbilt.grader.runner"
version = "0.0.1"

/** Groovy file chooser for picking account file or root directory for grading. */
apply(from = "groovy.gradle")
val fileChooser: groovy.lang.Closure<Any?> by extra

/** Property names must match those in gradle.properties. */
val repoName: String by project
val repoDir: String by project
val projDir: String by project
val accounts: String by project
val cloneCmd: String by project
val deleteCmd: String by project
val pingCmd: String by project
val commitCmd: String by project
val pushCmd: String by project

/** Task groups. */
val toolsGroupName = "tools"
val batchGroupName = "grader"
val testGroupName = "test"

/** Batch task names. */
val cloneTaskName = "clone"
val gradeTaskName = "grade"
val pingTaskName = "ping"
val deleteTaskName = "delete"
val commitTaskName = "commit"
val pushTaskName = "push"
val installTaskName = "install"

/** Other task names. */
val validatePropertiesTaskName = "validate properties"
val validateAccountsTaskName = "validate accounts"

/** Required names defined in grader plugin (DO NOT CHANGE) */
val runnerTask = "runAutograder"
val installerDirName = "installer"
val graderDirName = "autograder"
val submissionDirName = "AUTOGRADER_SUBMISSION"
val gradeFileName = "GRADE"
val summaryFileName = "SUMMARY"
val instrumentedTestFileName = "test.apk"
val unitTestFileName = "test.jar"
val sdkEnvVar = "ANDROID_HOME"

/** Local files (DO NOT CHANGE) */
val propertiesFileName = "gradle.properties"
val linuxGradleCmd = "gradlew" // Linux and iOS
val windowsGradleCmd = "gradlew.bat"
val logDir = "logs"

val gradleCmdArguments = "-Pandroid.debug.obsoleteApi=true"
val padChar = '+'
val bannerPattern = "$padChar"
val sshRegexPatternOld = "(git@gitlab.com):([^/]*)/([^ ]*)\\.git\\s*(.*)".toRegex()
val accountPattern = "\\s*([^:|]+)[|:]\\s*(.*)$".toRegex()
val lineLength = 80
val windowsMaxPathLength = 255
val accountsSeparator = '|'

/** Detects lines that start with # */
val String.isComment: Boolean
    get() = "^\\s*#.*$".toRegex().matches(this)

// Load local properties for private values.
val localProperties = Properties().apply {
    load(file("local.properties").inputStream())
}

/**
 * Maps a property string to its value in the following order of
 * descending order of precedence: project | local | environment
 */
val String.prop: String?
    get() = localProp ?: project.properties[this] as? String ?: getenv

val String.localProp: String?
    get() = localProperties.getProperty(this@localProp)

val String.getenv: String?
    get() = System.getenv(this)

/** Maps a repository to it's contained project directory */
val File.proj: File
    get() = if (projDir.isBlank()) {
        this
    } else {
        File(this, projDir)
    }

val File.isGradableProject: Boolean
    get() = isGradleProject && isGraderInstalled

val File.isGraderInstalled: Boolean
    get() = File(this, graderDirName).let {
        File(it, unitTestFileName).exists() || File(it, instrumentedTestFileName).exists()
    }

val File.isGradleProject: Boolean
    get() = isDirectory
            && File(this, "build.gradle").isFile
            && File(this, "settings.gradle").isFile

val File.isTargetProject: Boolean
    get() = endsWith(projDir) && isGradleProject

data class Skipped(val project: File, val reason: String)
data class Failed(val project: File, val reason: Exception)
data class Succeeded(val project: File, val grade: String, val Summary: String)

tasks.register<DefaultTask>(pingTaskName) {
    group = batchGroupName
    description = "Ensures that all remote repositories listed in the $accounts file are accessible."
    doFirstWithLogging {
        ping()
    }
}

tasks.register<DefaultTask>(commitTaskName) {
    group = batchGroupName
    description = "Adds and commits changes for all entries listed in the $accounts file."
    doFirstWithLogging {
        gitCommit()
    }
}

tasks.register<DefaultTask>(pushTaskName) {
    group = batchGroupName
    description = "Pushes commits for all entries listed in the $accounts file."
    doFirstWithLogging {
        gitPush()
    }
}

tasks.register<DefaultTask>(cloneTaskName) {
    group = batchGroupName
    description = "Clones remote projects listed in the $accounts file."

    doFirstWithLogging {
        clone()
    }
}

/**
 * Task used to (re-)install grader into all projects from accounts list
 */
tasks.register<DefaultTask>(installTaskName) {
    group = batchGroupName
    description = "Installs the grader files located in $installerDirName/ into the" +
            "\n$projDir directory of each entry" +
            "\nlisted in the $accounts file."

    doFirstWithLogging {
        install(file(accounts), overwrite = true)
    }
}

/**
 * Task the pops up a choose folder dialog and then runs the grader
 * for all installed projects found below the specified root directory.
 */
tasks.register<DefaultTask>(gradeTaskName) {
    group = batchGroupName
    description = "Runs the $projDir project grader for each" +
            "\nentry listed in the $accounts file."

    doFirstWithLogging {
        grade(file(accounts))
    }
}

/**
 * Task the pops up a choose folder dialog and then runs the grader
 * for all installed projects found below the specified root directory.
 */
tasks.register<DefaultTask>(deleteTaskName) {
    group = batchGroupName
    description = "Deletes all cloned repositories listed in the $accounts file."

    doFirstWithLogging {
        delete(file(accounts))
    }
}

fun Task.gitCommit(source: File = file(accounts)) {
    printBanner("Commit ${source.name}")

    getRepos(source).forEach { file ->
        require(file.isDirectory) { "$file: not a directory." }
        execCmd(commitCmd, listOf("."), file)
    }
}

fun Task.gitAdd(source: File = file(accounts)) {
    printBanner("Add ${source.name}")
}

fun Task.gitPush(source: File = file(accounts)) {
    printBanner("Push ${source.name}")

    getRepos(source).forEach { file ->
        require(file.isDirectory) { "$file: not a directory." }
        execCmd(pushCmd, inDir = file)
    }
}

fun Task.ping() {
    printBanner("Validating remote repositories")

    check(!pingCmd.isBlank()) {
        "$propertiesFileName: ping property must be specified."
    }

    getAccounts(mustExist = false).forEach { (repo) ->
        execCmd(pingCmd, listOf(repo))
    }
}


fun Task.clone() {
    printBanner("Cloning repositories")

    check(!cloneCmd.isBlank()) {
        "$propertiesFileName: $cloneCmd property must be specified."
    }

    check(!repoDir.isBlank()) {
        "$propertiesFileName: repoDir property must be specified."
    }

    val cloneFile = file(accounts)
    check(cloneFile.isFile) { "${cloneFile.name} not found" }

    getAccounts(mustExist = false).forEach { cols ->
        execCmd(format = cloneCmd, list = cols, inDir = rootDir, filter = {
            if (!file(it.last().trim('/')).exists()) {
                true
            } else {
                log("Skipping: ${it.last()} already exists.")
                false
            }
        }) {
            with(file(it.last()).proj) {
                if (!isGradleProject) {
                    log("$path: not a Gradle project.")
                } else if (!isGraderInstalled) {
                    log("$path: autograder is not installed.")
                }
            }
        }
    }
}

fun Task.install(source: File, overwrite: Boolean = false) {
    printBanner("Install ${source.name}")

    getRepos(source).forEach {
        it.proj.installGrader(overwrite)
    }
}

fun File.installGrader(overwrite: Boolean = false) {
    require(isDirectory) { "$path: directory does not exist." }

    if (isGraderInstalled && !overwrite) {
        return
    }

    val dir = File(this, installerDirName)
    if (dir.isDirectory) {
        log("Deleting existing grader dir $dir ...")
        dir.deleteRecursively()
    }

    log("Installing grader into $this ...")

    val fromDir = file(installerDirName)
    check(fromDir.isDirectory && !fromDir.listFiles().isNullOrEmpty()) {
        "Missing $graderDirName directory in ${relPath()} project and no default " +
                "$graderDirName directory exists to clone into that project."
    }

    copy {
        from(fromDir)
        into(this@installGrader)
    }
}

fun File.deleteGrader() {
    require(isDirectory) { "$path: directory does not exist." }

    File(this, installerDirName).run {
        if (isDirectory) {
            log("Deleting existing grader dir $this ...")
            deleteRecursively()
        }
    }
}

fun Task.delete(file: File) {
    printBanner("DELETING PROJECTS in ${file.name}")

    require(file.exists()) { "$path: directory does not exist." }

    val repos = getRepos(file)

    repos.filter {
        if (it.exists()) {
            log(it.relPath())
            true
        } else {
            log("${it.relPath()}: does not exist (skipping)")
            false
        }
    }.map { repo ->
        printDivider(repo.path)
        execCmd(deleteCmd, mapOf("repo" to repo.path))
    }
}

/**
 * Loads accounts file contents and displays a chooser dialog
 * for user to select which accounts to use for an operation.
 */
fun Task.getRepos(source: File = file(accounts), mustExist: Boolean = true, showList: Boolean = true): List<File> {

    val projects = when {
        source.isDirectory -> {
            val (projects, skipped) = buildProjectList(source)

            skipped.forEach {
                log("Skipping ${it.project.relPath()} reason: ${it.reason}")
            }

            projects
        }
        source.isFile -> {
            getAccounts(from = source, showList = true, mustExist = mustExist).map {
                file("$repoDir/${it[1]}")
            }
        }
        else -> {
            throw Exception("$source: does not exist")
        }
    }

    check(projects.isNotEmpty()) { "No gradable projects found!" }

    log("Found ${projects.size} gradable projects:")
    log()

    return projects
}

class AccountChooser(private val list: List<List<String>>,
                     private val title: String,
                     private val action: String,
                     private val mustExist: Boolean = true) {
    private val noBorder = EmptyBorder(0, 0, 0, 0)
    private val cache = file("$logDir/$accounts.log")

    fun show(): List<List<String>> {
        val model = DefaultListModel<JCheckBox>().apply {
            list.forEach {
                addElement(JCheckBox(it[1]).apply {
                    isEnabled = !mustExist || file("$repoDir/${it[1]}").isDirectory
                    isFocusable = isEnabled
                    isRequestFocusEnabled = isEnabled
                })
            }
            restore()
        }

        val checkBoxList = JCheckBoxList(model).apply {
            visibleRowCount = list.size
        }

        val outerPanel = JPanel().apply {
            add(JScrollPane(checkBoxList))
            layout = BoxLayout(this, BoxLayout.Y_AXIS)
        }

        val selectAllCheckBox = JCheckBox(object : AbstractAction("Select/Deselect all") {
            override fun actionPerformed(e: ActionEvent?) {
                (e?.source as? JCheckBox)?.apply {
                    model.elements().toList().forEach {
                        it.isSelected = isSelected
                    }
                }
                checkBoxList.repaint()
            }
        })

        val options = arrayOf(action, "Cancel")
//        JOptionPane.showOptionDialog(
//                null,
//                message,
//                title,
//                JOptionPane.OK_OPTION,
//                JOptionPane.PLAIN_MESSAGE,
//                null,
//                options,
//                options[0]);

        val listPane = JOptionPane(
                arrayOf(selectAllCheckBox, outerPanel),
                JOptionPane.PLAIN_MESSAGE,
                JOptionPane.OK_CANCEL_OPTION,
                null,
                options,
                null)
        listPane.createDialog(null, title).apply {
            isResizable = true
            isVisible = true
        }

        model.save()

        return when (listPane.value) {
            JOptionPane.OK_OPTION -> {
                list.filterIndexed { index, _ ->
                    model.elementAt(index).isSelected
                }
            }
            else -> emptyList()
        }
    }

    private fun DefaultListModel<JCheckBox>.save() {
        if (cache.isFile) {
            cache.delete()
        } else {
            cache.parentFile.mkdirs()
        }

        cache.createNewFile()

        elements().toList().forEach {
            if (it.isSelected) {
                cache.appendText("${it.text}\n")
            }
        }
    }

    fun DefaultListModel<JCheckBox>.restore() {
        val selected = if (cache.isFile) {
            cache.readLines().filterNot { it.isBlank() }
        } else {
            emptyList()
        }

        elements().toList().forEach { checkBox ->
            checkBox.isSelected = selected.contains(checkBox.text)
        }
    }

    private inner class JCheckBoxList(model: ListModel<JCheckBox>) : JList<JCheckBox>(model) {
        inner class SelectionModel : DefaultListSelectionModel() {
            override fun isSelectedIndex(index: Int): Boolean {
                return super.isSelectedIndex(index) && model.getElementAt(index).isEnabled
            }
        }

        init {
            selectionModel = SelectionModel()
            selectionMode = ListSelectionModel.MULTIPLE_INTERVAL_SELECTION
            cellRenderer = ListCellRenderer<JCheckBox> { list, value, index, isSelected, _ ->
                value.componentOrientation = list.componentOrientation
                value.background = if (isSelected) selectionBackground else list.background
                value.foreground = if (isSelected) selectionForeground else list.foreground
                value.isEnabled = list.isEnabled && model.getElementAt(index).isEnabled
                if (!model.getElementAt(index).isEnabled) {
                    value.isSelected = false
                    value.isFocusPainted = false
                    value.isFocusable = false
                }
                value.font = list.font
                value.isFocusPainted = false
                value.isBorderPainted = true
                value.border = if (isSelected) {
                    UIManager.getBorder("List.focusCellHighlightBorder")
                } else {
                    EmptyBorder(1, 1, 1, 1)
                }
                value
            }
            addListeners()
        }

        private fun addListeners() {
            addMouseListener(object : MouseAdapter() {
                override fun mousePressed(e: MouseEvent) {
                    val index = locationToIndex(e.point)
                    with(model.getElementAt(index)) {
                        if (index != -1 && isEnabled) {
                            isSelected = !isSelected
                            repaint()
                        }
                    }
                }
            })

            addKeyListener(object : KeyAdapter() {
                override fun keyPressed(e: KeyEvent) {
                    val index = selectedIndex
                    if (index != -1 && e.keyCode == KeyEvent.VK_SPACE) {
                        val newVal = !(model.getElementAt(index) as JCheckBox).isSelected
                        selectedIndices.map {
                            model.getElementAt(it) as JCheckBox
                        }.filter {
                            it.isEnabled
                        }.forEach {
                            it.isSelected = newVal
                            repaint()
                        }
                    }
                }
            })
        }
    }
}

fun Task.getAccounts(from: File = file(accounts),
                     title: String? = null,
                     showList: Boolean = true,
                     mustExist: Boolean = true,
                     maxCols: Int = 2): List<List<String>> {
    require(from.exists()) { "${from.name} not found." }
    require(from.isFile) { "${from.name}: is not a file." }

    val lines = from.readLines(Charset.defaultCharset()).filterNot {
        it.isEmpty() || it.isComment
    }.map {
        it.trim()
    }

    val list = lines.map { row ->
        val cols = row
                .split(accountsSeparator)
                .map { it.trim() }
                .filterNot { it.isBlank() }

        require(cols.size in 1..maxCols) {
            "$accounts: line [$row] does not match <account>[/repoName] [| destDir]"
        }

        val split = cols[0].split("/")

        require(split.size in 1..2) {
            "$accounts: first column does not match <account>[/repoName]"
        }

        val account = split[0]
        val repo = split.getOrElse(1) { repoName }

        when (cols.size) {
            1 -> listOf("${account}/${repo}", account)
            2 -> listOf("${account}/${repo}", cols[1])
            else -> throw Exception("$accounts: line [$row] has more than 2 columns.")
        }
    }.filter {
        !mustExist || file("$repoDir/${it[1]}").isDirectory
    }

    require(list.isNotEmpty()) { "${from.name}: no account entries to process." }

    val sorted = list.sortedBy {
        it[1]
    }

    // Check for uniqueness
    list.forEachIndexed { i, row ->
        val rest = list.drop(i + 1)
        require(rest.firstOrNull { it == row } == null) {
            "$accounts: line [${lines[i]}] has a duplicate entry"
        }

        var dup = rest.indexOfFirst {
            it.first().split("/").first() == row.first().split("/").first()
        }
        require(dup == -1) {
            "$accounts: line [${lines[i]}] and line [${lines[i + 1 + dup]}] have the same git account."
        }

        dup = rest.indexOfFirst { it.last() == row.last() }
        require(dup == -1) {
            "$accounts: line [${lines[i]}] line [${lines[i + 1 + dup]}] have the same destination directory [${row.last()}]."
        }
    }

    return if (showList) {
        val action = (if (title.isNullOrBlank()) name else title).let { it.capitalize() }
        AccountChooser(
                list = sorted,
                title = action,
                action = action,
                mustExist = mustExist).show()
    } else {
        sorted
    }
}

/**
 * Runs the grader for each project in [projects] and produces summary output.
 */
fun Task.grade(source: File) {
    printBanner("BATCH GRADING STARTED")

    // Android SDK path must be set.
    val sdkDir = "sdk.dir".prop
            ?: sdkEnvVar.getenv
            ?: throw IllegalStateException("Either the sdk.dir Gradle property or the " +
                    "$sdkEnvVar environment variable must be set to the Android SDK " +
                    "directory")

    val projects = getRepos(source, mustExist = true).map { it.proj }

    projects.forEach {
        log(it.relPath())
    }

    val succeeded = mutableListOf<Succeeded>()
    val failed = mutableListOf<Failed>()

    projects.forEach { project ->
        try {
            runner(project, sdkDir)

            "${project.absolutePath}/$submissionDirName".let {
                Succeeded(project,
                        file("$it/$gradeFileName").readText(),
                        file("$it/$summaryFileName").readText())
            }.also {
                succeeded.add(it)
            }
        } catch (e: Exception) {
            failed.add(Failed(project, e))
            log("[ERROR] Grading failed for project ${project.relPath()}: ${e.message}")
        }
    }

    if (succeeded.isNotEmpty()) {
        printBanner("GRADED PROJECTS")
        succeeded.forEach {
            log("${it.project.relPath()}: ${it.grade}%")
        }
    }

    if (failed.isNotEmpty()) {
        printBanner("FAILED PROJECTS")
        failed.forEach {
            log("${it.project.relPath()}: ${it.reason.message}")
        }
    }

    printBanner("BATCH GRADING SUMMARY")
    log("Processed: ${projects.size}")
    log("Succeeded: ${succeeded.size}")
    log("Failed:    ${failed.size}")
    printBanner("BATCH GRADING COMPLETED")
}

fun runner(project: File, sdkDir: String) {
    printBanner("GRADING project ${project.path}")

    check(project.isDirectory) { "The project $project is NOT a directory." }

    if (!project.isGraderInstalled) {
        log("Installing grader ...")
        project.installGrader()
    }

    val gradleCommand = project.getGradleCommand()
    exec {
        workingDir = project
        commandLine(gradleCommand, runnerTask, "-Psdk.dir='$sdkDir'")
    }

    log()
}

/**
 * Finds and returns all gradle projects within [topDir] containing grader
 * unit and/or instrumented tests. Any skipped project directories are also
 * returned along with a reason why they were skipped.
 */
fun buildProjectList(topDir: File): Pair<List<File>, List<Skipped>> {
    val projects = mutableListOf<File>()
    val skipped = mutableListOf<Skipped>()

    topDir.walk().filter {
        it.isDirectory && it.endsWith(projDir) && it.isGradleProject
    }.forEach { dir ->
        when {
            projectDirHasTooLongPaths(dir) -> {
                skipped.add(Skipped(dir, "Path length exceed OS limit of $windowsMaxPathLength"))
            }
            !dir.isGraderInstalled && !rootDir.isGraderInstalled -> {
                skipped.add(Skipped(dir, "Grader not installed"))
            }
            else -> {
                projects.add(dir)
            }
        }
    }

    return Pair(projects, skipped)
}

/**
 * Finds and returns all gradle projects within [File] receiver containing
 * grader unit and/or instrumented tests. Any skipped project directories
 * are also returned along with a reason why they were skipped.
 */
fun File.listRepos(): List<File> {
    require(isDirectory) { "$this: directory does not exist." }

    return walk().filter {
        it.isTargetProject
        it.endsWith(projDir) && it.isGradleProject
    }.toList()
}

fun Task.doFirstWithLogging(block: () -> Unit) {
    doFirst {
        val logFile = file("$logDir/$name.log")
        logFile.parentFile.mkdirs()

        val writer = logFile.outputStream().writer()

        val outputListener = StandardOutputListener {
            writer.append(it)
            writer.flush()
        }

        try {
            with(tasks.getByName(name)) {
                logging.addStandardOutputListener(outputListener)
                logging.addStandardErrorListener(outputListener)
            }

            block()
        } catch (t: Throwable) {
            log("Task '$name' encountered an exception: ${t.message}")
            throw t
        } finally {
            writer.flush()
            writer.close()
            with(tasks.getByName(name)) {
                logging.removeStandardOutputListener(outputListener)
                logging.removeStandardErrorListener(outputListener)
            }
        }
    }
}

/**
 * Add -Psdk.dir so that gradle will be able to find sdk dir for all projects.
 * @return the OS dependent gradle script command (gradle.bat or gradlew).
 */
fun File.getGradleCommand(): String {
    return if (isWindows()) {
        windowsGradleCmd
    } else {
        // Make sure that gradlew is executable.
        val cmd = File(this, linuxGradleCmd)
        if (!cmd.canExecute()) {
            cmd.setExecutable(true)
        }
        "./$linuxGradleCmd"
    }
}

/**
 * Executes formatted command(s) substituting ${i} for list[i].
 */
fun execCmd(format: String,
            list: List<String>,
            inDir: File = rootDir,
            filter: (List<String>) -> Boolean = { true },
            after: (List<String>) -> Unit = {}) {
    val map = list.mapIndexed { i, v -> i.toString() to v }.toMap()
    execCmd(format, map, inDir, filter, after)
}

/**
 * Executes formatted command(s) substituting ${key} for map[key].
 */
fun execCmd(format: String,
            map: Map<String, String> = emptyMap(),
            inDir: File = rootDir,
            filter: (List<String>) -> Boolean = { true },
            after: (List<String>) -> Unit = {}) {
    format.resolve(map)
            .split(";")
            .filterNot {
                it.isBlank()
            }.forEach { cmd ->
                log(cmd)

                // Splits command into file and non-file parts
                // where each file must be enclosed in single quotes.
                val parts = cmd
                        .split("'")
                        .filterNot { it.isBlank() }
                        .map { it.trim() }
                val args = parts[0].split("[\\s]+".toRegex()).toMutableList()
                val rest = parts.takeLast(parts.size - 1)
                val execArgs = args.apply { addAll(rest) }
                if (filter(execArgs)) {
                    try {
                        exec {
                            println("WorkingDir = $inDir")
                            workingDir = inDir
                            commandLine(execArgs)
                        }
                    } catch (e: Exception) {
                        log("${args[0]} encountered an exception: ${e.message}")
                    }

                    after(execArgs)
                }
            }
}

fun String.resolve(columns: List<String>): String =
        resolve(columns.mapIndexed { i, value -> Pair(i.toString(), value) }.toMap())

fun String.resolve(map: Map<String, String> = emptyMap()): String {
    val pattern = """.*(\$\{[^\}]+\}).*""".toRegex()
    var string = this
    while (pattern.matches(string)) {
        val matchResult = pattern.matchEntire(string)
        val match = matchResult?.groupValues?.get(1) ?: break
        val name = match.substring(2, match.lastIndex)
        val value = map[name] ?: name.prop
        ?: throw Exception("$string: property '$name' is not defined.")
        string = string.replace(match, value)
    }
    return string
}

/**
 * Displays a banner title between 2 lines of a repeated character pattern.
 */
fun printBanner(title: String, vararg args: Any?) {
    log()
    val text = title.format(*args)
    log("$padChar".repeat(lineLength / bannerPattern.length))
    val padding = max((lineLength - text.length) / 2.0, 5.0).toInt()
    log(text.padStart(padding + text.length, ' ').padEnd(lineLength, ' '))
    log("$padChar".repeat(lineLength / bannerPattern.length))
    log()
}

fun printDivider(title: String? = null, vararg args: Any?) {
    if (title.isNullOrBlank()) {
        log("$padChar".repeat(lineLength))
    } else {
        val text = title.format(*args)
        val padding = max((lineLength - text.length) / 2.0, 5.0).toInt()
        log(text.padStart(padding + text.length, padChar).padEnd(lineLength, padChar))
    }
    log()
}

fun startGroup(title: String? = null, vararg args: Any?) {
    if (title.isNullOrBlank()) {
        printDivider()
    } else {
        printBanner(title, *args)
    }
}

fun endGroup(title: String? = null, vararg args: Any?) {
    log()
    printDivider(title, args)
}

/**
 * Prepends a prefix string to all logging messages.
 */
fun log(msg: String = "", vararg args: Any?) {
    if (args.isNotEmpty()) {
        println(msg.format(*args))
    } else {
        println(msg)
    }
}

/**
 * Determines if the passed directory [dir] contains any path lengths that
 * exceed the maximum OS path length. Currently, only checked for Windows 255
 * path length limit.
 */
fun projectDirHasTooLongPaths(dir: File): Boolean =
        isWindows() && fileTree(dir).filter {
            it.absolutePath.length > windowsMaxPathLength
        }.count() > 0

/**
 * @return true if the current OS is windows.
 */
fun isWindows(): Boolean = "windows".compareTo(System.getProperty("os.name"), false) == 0

fun File.relPath(): String = relativePath(this)

fun relativePath(dir: File): String = dir.path.removePrefix("${rootDir.path}/")

fun chooseFile(block: (File) -> Unit) {
    val selectedFile = fileChooser() as String
    if (selectedFile.isNotBlank()) {
        block(file(selectedFile))
    } else {
        log("No file or directory was selected")
    }
}

tasks {
//    val save_grades by registering {
//        group = "test"
//        doFirstWithLogging {
//            getAccounts(file("GRADES"), showList = true, mustExist = true, maxCols = 3)
//                    .forEach { (_, dir, grade) ->
//                        val proj = file("$repoDir/$dir").proj
//                        check(proj.isGradableProject) {
//                            "$proj: does not exist or is not a gradable project"
//                        }
//
//                        val file = File(proj, "GRADE.txt")
//                        val status = if (file.isFile) {
//                            val oldGrade = file.readText().trim()
//                            if (oldGrade == grade.trim()) {
//                                "already has grade $grade"
//                            } else {
//                                "changing grade from $oldGrade to $grade"
//                            }
//                        } else {
//                            "setting grade to $grade"
//                        }
//                        file.writeText(grade)
//                        log("${proj.path.padStart(50, ' ')}: $status")
//                    }
//        }
//    }

    val choose_accounts by registering {
        group = "test"
        doFirst {
            getAccounts(mustExist = false).forEach {
                println(it[1])
            }
        }
    }

    val string_resolve by registering {
        group = "test"
        doFirst {
            println("RESULT: ${cloneCmd.resolve(mapOf("0" to "proj_path", "1" to "repo_dir"))}")
        }
    }

    val get_property by registering {
        group = "test"
        doFirst {
            val name = "test_prop"
            println("$name: [${name.prop}]")
        }
    }
}
