# grader

Vanderbilt University Gradle project used to clone and grade student 
Gradle project assignments.

# Installation

To install the grader and all student projects

1. Clone or copy this project to your computer.
1. Open the project in Android Studio or Intellij Idea.
1. Open the ACCOUNTS file in the editor and add a line for each remote
student git repository using the following format:<br><br>
   ```
   <account-id> [unique-dir-name] 
   ```
   If the optional ***unique-dir-name*** target directory is not specified, 
   ***account-id*** will be used.
1. Open ***gradle.properties*** in the editor and set the ***repoName***
to the common git repository name used by all students and set the
***repoDir*** property to the relative path to the student's Gradle 
assignment project. 
1. Navigate to the Gradle tool window.
1. Double-click the ***Tasks/tools/clone*** task.

Alternatively, to run the ***cloneProjects*** task from the command line

1. Open a terminal window in this project's root directory.
1. Run the command<br><br>
   ```
     Linux/iOS:   gradle clone
     Windows:    ./gradlew clone
   ```

Projects will be cloned into the ***repos*** directory located in the 
project's root directory. As each project is cloned, a check is made to 
see if the grader has been pre-installed in the cloned project and if 
not, it will be automatically installed.

# Viewing ***clone*** results

A full log of all cloning results will be displayed in the console window
and will also be saved in the file file ***clone.log*** located in the 
***grader*** project's root directory.

# Grading all cloned projects

To grade all assignment projects that were cloned using the list in the
accounts file as specified by the ***gradle.properties*** ACCOUNTS property

1. Open this project in Android Studio or Intellij IDEA.
1. Navigate to the Gradle tool window.
1. Double-click the ***Tasks/tools/grade*** task.

Alternatively, to run the ***grade*** task from the command line

1. Open a terminal window in this project's root directory.
1. Run the command<br><br>
   ```
   Linux/iOS:   gradle grade
   Windows:    ./gradlew grade
   ```

# Viewing ***grade*** results

A full log of all grading results will be displayed in the console window
while the grader is running. Additionally, a copy of all logging output
is also saved in file ***grade.log*** located in the ***grader*** 
project's root directory.

# Grading selectively

You can also selectively choose which assignments to grade (one or multiple)
by running the ***gradeSelectively*** task in the ***tools*** task group. When 
run, this task will display a file chooser dialog all for selecting any 
file containing a custom list of student account IDs or any directory 
to be scanned for student projects to grade. To run this task

1. Open this project in Android Studio or Intellij IDEA.
1. Navigate to the Gradle tool window.
1. Double-click the ***Tasks/tools/gradeSelectively*** task.

Alternatively, to run the ***gradeSelectively*** task from the command line

1. Open a terminal window in this project's root directory.
1. Run the command <br><br>
   ```
   Linux/iOS:   gradle gradeSelectively
   Windows:    ./gradlew gradeSelectively
   ```

# Viewing ***gradeSelectively*** Results

A full log of all selective grading results will be displayed in the console 
window while the grader is running. Additionally, a copy of all logging output
is also saved in file ***gradeSelectively.log*** located in the ***grader*** 
project's root directory.

# Customization

The grader cloning and grading tasks can be customized by setting custom
property values in the ***gradle.properties*** file located in the
***grader*** project's root directory. The following table describes each
property in detail.
<br><br>

| Property | Description | Default | Usage Example |
|:---------|:------|:------|:------|
|repoName|Repository name|(required)|repoName=cs-891-fall-2019|
|repoDir|Relative path to the target assignment project within the repository.|.|repoDir=assignments/assignment2c|
|accounts|File containing list of unique repository account names|./STUDENTS|accounts=./STUDENTS|
|cloneDir|Destination directory for all cloned repositories|./REPOS|cloneDir=./REPOS|
|cloneCommand|Command clone or copy repositories into the specified directory|git module add git@gitlab.com:${account}/${repoName}.git ${cloneDir}/${destDir}|cloneCommand=cp -r /tmp/students/${account} ${cloneDir}/${account}-new

You can customize the ***cloneCommand*** to perform any type of operation
as long as you use one or more named placeholders (e.g. ${*repoName*}) in
positions that produce a syntactically correct command. 
